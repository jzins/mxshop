def ipConfig(oldip, oldport, newip, newport, py_dir):
    for f in py_dir:
        file_data = ""
        file_name = f.split("\\")[5]
        with open(f, "r", encoding="utf-8") as fp:
            for line in fp:
                if oldip in line:
                    line = line.replace(oldip, newip)
                    print(file_name + " -> " + newip)
                file_data += line

        with open(f, "w", encoding="utf-8") as fp2:
            fp2.write(file_data)


if __name__ == '__main__':
    f_list = ["E:\\zhuomian\\project\\python\\mxshop_srv\\goods_srv\\server.py",
              "E:\\zhuomian\\project\\python\\mxshop_srv\\inventory_srv\\server.py",
              "E:\\zhuomian\\project\\python\\mxshop_srv\\order_srv\\server.py",
              "E:\\zhuomian\\project\\python\\mxshop_srv\\user_srv\\server.py",
              "E:\\zhuomian\\project\\python\\mxshop_srv\\userop_srv\\server.py"]
    ipConfig(oldip="192.168.1.101", oldport="", newip="192.168.1.106", newport="", py_dir=f_list)
