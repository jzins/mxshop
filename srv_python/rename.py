# 批量修改文件名
# 批量修改图片文件名
import os
import re
import sys


def AllFileRoute(path):
    fileRouter_dict = dict()
    fileRouter_list = list()
    for root, _, files in os.walk(path):
        # print(root)  # 所有的子目录

        # print(dirs) # 所有的目录名称

        # print(files)  # 遍历所有文件的绝对地址
        for file in files:
            path = os.path.join(root, file).replace(f"\\{file}", "")
            # print(path)
            fileRouter_dict[path] = "1"
            fileRouter_list = list(fileRouter_dict)
    return fileRouter_list


def renameall(file):
    # 批量修改文件名

    fileList = os.listdir(file)  # 待修改文件夹
    print("修改前：" + str(fileList))  # 输出文件夹中包含的文件
    currentpath = os.getcwd()  # 得到进程当前工作目录
    os.chdir(file)  # 将当前工作目录修改为待修改文件夹的位置
    for fileName in fileList:  # 遍历文件夹中所有文件
        newName = fileName.replace(".mp4", "")
        pat = ".+\.(mp4|jpg|png)"  # 匹配文件名正则表达式
        pattern = re.findall(pat, fileName)  # 进行匹配
        try:
            os.rename(fileName, (newName + '.' + pattern[0]))  # 文件重新命名
        except Exception as e:
            pass
    print("---------------------------------------------------")
    os.chdir(currentpath)  # 改回程序运行前的工作目录
    sys.stdin.flush()  # 刷新
    print("修改后：" + str(os.listdir(file)))  # 输出修改后文件夹中包含的文件


if __name__ == '__main__':
    fileAllList = []
    fileList = (os.listdir(f"E:\\zhuomian\\go"))  # 待修改文件夹
    # fileList = [file for file in fileList if file not in ["021-gogogogo-2023", "课程资料"]] # 不修改的子目录
    for i in fileList:
        fileAllList += AllFileRoute(f"E:\\zhuomian\\go\\{i}")
    for j in fileAllList:
        # print(j)
        renameall(j)
