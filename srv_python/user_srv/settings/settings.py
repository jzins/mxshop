import nacos
import json
from playhouse.pool import PooledMySQLDatabase
from playhouse.shortcuts import ReconnectMixin
from loguru import logger


# 使用peewee的连接池，使用ReconnectMixin来防止出现连接断开查询失败
class ReconnectMysqlDatabase(ReconnectMixin, PooledMySQLDatabase):
    # python的mro super(ReconnectMixin, self)不是调用父类(object)
    pass


NACOS = {
    "Host": "192.168.10.130",
    "Port": 8848,
    "NameSpace": "d35c6a37-2a22-465d-944c-ef3a58c9bee0",
    "User": "nacos",
    "Password": "nacos",
    "DataId": "user-srv_python.json",
    "Group": "dev",
}
client = nacos.NacosClient(f'{NACOS["Host"]}:{NACOS["Port"]}', namespace=NACOS["NameSpace"], username=NACOS["User"],
                           password=NACOS["Password"])
data = client.get_config(NACOS["DataId"], NACOS["Group"])
data = json.loads(data)
logger.info(data)


def update_cfg(args):
    print("配置文件产生变化")
    print(args)


# 主IP
HOST = data["host"]
# consul的配置
CONSUL_HOST = data["consul"]["host"]
CONSUL_PORT = data["consul"]["port"]

# 服务相关的配置
SERVICE_NAME = data["name"]
SERVICE_TAGS = data["tags"]

# 防止出错 指定名称
DB = ReconnectMysqlDatabase(database=data["mysql"]["db"], host=data["mysql"]["host"], port=data["mysql"]["port"],
                            user=data["mysql"]["user"],
                            password=data["mysql"]["password"])
