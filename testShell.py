import os
import asyncio

user_srv = "E:\\zhuomian\\project\\mxshop\\srv_golang\\user_srv\\main.go"
userop_srv = "E:\\zhuomian\\project\\mxshop\\srv_golang\\userop_srv\\main.go"
goods_srv = "E:\\zhuomian\\project\\mxshop\\srv_golang\\goods_srv\\main.go"
order_srv = "E:\\zhuomian\\project\\mxshop\\srv_golang\\order_srv\\main.go"
inventory_srv = "E:\\zhuomian\\project\\mxshop\\srv_golang\\inventory_srv\\main.go"

user_web = "E:\zhuomian\project\mxshop\web_golang\order-web\main.go"
userop_web = "E:\\zhuomian\\project\\mxshop\\web_golang\\userop-web\\main.go"
goods_web = "E:\\zhuomian\\project\\mxshop\\web_golang\\goods-web\\main.go"
order_web = "E:\\zhuomian\\project\\mxshop\\web_golang\\order-web\\main.go"
oss_web = "E:\\zhuomian\\project\\mxshop\\web_golang\\oss-web\\main.go"
server = [user_srv, userop_srv, goods_srv, order_srv, inventory_srv, user_web, userop_web, goods_web, order_web,
          oss_web]


async def shell(cmd):
    os.system(f"start cmd /k {cmd}")


async def main():
    for cmd in server:
        asyncio.create_task(shell(cmd))


if __name__ == '__main__':
    asyncio.run(main())
