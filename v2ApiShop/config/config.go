package config

type UserSrvConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
	Name string `mapstructure:"name" json:"name"`
}
type UserOpSrvConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
	Name string `mapstructure:"name" json:"name"`
}
type GoodsSrvConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
	Name string `mapstructure:"name" json:"name"`
}
type OrderSrvConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
	Name string `mapstructure:"name" json:"name"`
}
type InventorySrvConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
	Name string `mapstructure:"name" json:"name"`
}
type OssConfig struct {
	ApiKey      string `mapstructure:"key" json:"key"`
	ApiSecrect  string `mapstructure:"secrect" json:"secrect"`
	Host        string `mapstructure:"host" json:"host"`
	CallBackUrl string `mapstructure:"callback_url" json:"callback_url"`
	UploadDir   string `mapstructure:"upload_dir" json:"upload_dir"`
}
type JaegerConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
	Name string `mapstructure:"name" json:"name"`
}
type JWTConfig struct {
	SigningKey string `mapstructure:"key" json:"key"`
}

type AliSmsConfig struct {
	ApiKey     string `mapstructure:"key" json:"key"`
	ApiSecrect string `mapstructure:"secrect" json:"secrect"`
}

type ConsulConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
}

type RedisConfig struct {
	Host   string `mapstructure:"host" json:"host"`
	Port   int    `mapstructure:"port" json:"port"`
	Expire int    `mapstructure:"expire" json:"expire"`
}
type AlipayConfig struct {
	AppID        string `mapstructure:"app_id" json:"app_id"`
	PrivateKey   string `mapstructure:"private_key" json:"private_key"`
	AliPublicKey string `mapstructure:"ali_public_key" json:"ali_public_key"`
	NotifyURL    string `mapstructure:"notify_url" json:"notify_url"`
	ReturnURL    string `mapstructure:"return_url" json:"return_url"`
}

type ServerConfig struct {
	Name             string             `mapstructure:"name" json:"name"`
	Host             string             `mapstructure:"host" json:"host"`
	Port             int                `mapstructure:"port" json:"port"`
	Tags             []string           `mapstructure:"tags" json:"tags"`
	JWTInfo          JWTConfig          `mapstructure:"jwt" json:"jwt"`
	AliSmsInfo       AliSmsConfig       `mapstructure:"sms" json:"sms"`
	RedisInfo        RedisConfig        `mapstructure:"redis" json:"redis"`
	ConsulInfo       ConsulConfig       `mapstructure:"consul" json:"consul"`
	JaegerInfo       JaegerConfig       `mapstructure:"jaeger" json:"jaeger"`
	OssInfo          OssConfig          `mapstructure:"oss" json:"oss"`
	AliPayInfo       AlipayConfig       `mapstructure:"alipay" json:"alipay"`
	UserSrvInfo      UserSrvConfig      `mapstructure:"user-srv" json:"user-srv"`
	UserOpSrvInfo    UserOpSrvConfig    `mapstructure:"userop-srv" json:"userop-srv"`
	GoodsSrvInfo     GoodsSrvConfig     `mapstructure:"goods-srv" json:"goods-srv"`
	OrderSrvInfo     OrderSrvConfig     `mapstructure:"order-srv" json:"order-srv"`
	InventorySrvInfo InventorySrvConfig `mapstructure:"inventory-srv" json:"inventory-srv"`
}
type NacosConfig struct {
	Host      string `mapstructure:"host"`
	Port      uint64 `mapstructure:"port"`
	Namespace string `mapstructure:"namespace"`
	User      string `mapstructure:"user"`
	Password  string `mapstructure:"password"`
	DataId    string `mapstructure:"dataid"`
	Group     string `mapstructure:"group"`
}
