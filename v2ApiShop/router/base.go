package router

import (
	"github.com/gin-gonic/gin"
	v1user "v2ApiShop/api/user"
)

func InitBaseRouter(Router *gin.Engine) {
	BaseRouter := Router.Group("base")
	{
		BaseRouter.GET("captcha", v1user.GetCaptcha)
		BaseRouter.POST("send_sms", v1user.SendSms)
	}
}
