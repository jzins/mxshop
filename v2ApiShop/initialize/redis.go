package initialize

import (
	"fmt"
	goredislib "github.com/redis/go-redis/v9"
	"v2ApiShop/global"
)

func InitRedis() {
	global.RedisClient = goredislib.NewClient(&goredislib.Options{
		Addr: fmt.Sprintf("%s:%d", global.ServerConfig.RedisInfo.Host, global.ServerConfig.RedisInfo.Port),
		DB:   0,
	})
}
