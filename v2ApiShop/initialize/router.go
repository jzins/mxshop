package initialize

import (
	"github.com/gin-gonic/gin"
	"v2ApiShop/middlewares"
	router2 "v2ApiShop/router"
)

func Routers() *gin.Engine {
	Router := gin.Default()
	Router.GET("/health", func(c *gin.Context) {
		return
	})
	//配置跨域
	Router.Use(middlewares.Cors())
	//ApiGroup := Router.Group("/u/v1")
	router2.InitUserRouter(Router)   //用户相关
	router2.InitUserOpRouter(Router) //用户操作相关
	router2.InitBaseRouter(Router)   //验证码相关
	router2.InitGoodsRouter(Router)  //商品相关
	router2.InitOrderRouter(Router)  //订单相关
	router2.InitOssRouter(Router)    //oss相关
	return Router
}
