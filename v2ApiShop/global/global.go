package global

import (
	ut "github.com/go-playground/universal-translator"
	"github.com/go-redsync/redsync/v4"
	"github.com/redis/go-redis/v9"
	"v2ApiShop/config"
	v1goodsproto "v2ApiShop/proto/goods"
	v1inventory "v2ApiShop/proto/inventory"
	v1orderproto "v2ApiShop/proto/order"
	v1user "v2ApiShop/proto/user"
	v1userop "v2ApiShop/proto/userop"
)

var (
	Trans              ut.Translator
	ServerConfig       *config.ServerConfig = &config.ServerConfig{}
	UserSrvClient      v1user.UserClient
	UserOpSrvClient    v1userop.UserOpClient
	GoodsSrvClient     v1goodsproto.GoodsClient
	OrderSrvClient     v1orderproto.OrderClient
	InventorySrvClient v1inventory.InventoryClient
	//NacosConfig   *config.NacosConfig = &config.NacosConfig{}
	RedisClient *redis.Client
	RedisRs     *redsync.Redsync
)
