# 电商系统-mxshop

【随缘更新】

## 介绍

电商系统 - 分层式微服务架构，服务之间互相解耦。

server层向内提供grpc通讯，web层向外提供http通讯。

业务逻辑在web层开发，底层交互在server层开发。

所有服务集成nacos配置中心、consul注册服务发现、健康检查、负载均衡。

redis中间件，mysql存储，elasticsearch全文搜索。

jaeger链路追踪，sentnel[熔断,限流]。

API网关「kong」，敏捷开发「jenkins」。

## 电商系统总架构

>srv_python是用python写的server层「未实现elasticsearch」。

>srv_golang是用go语言写的server层,把前期用python写底层逻辑全部重构并优化。

web层点进[web_golang查看文档](https://gitee.com/jzins/mxshop/tree/master/web_golang)

srv层点进srv_python(即将弃用)或[srv_golang查看文档](https://gitee.com/jzins/mxshop/tree/master/srv_golang)

架构：https://www.processon.com/view/link/633904dee0b34d40be4e42ef
![输入图片说明](https://foruda.gitee.com/images/1675762632261815914/bdb3e817_5720251.png "电商系统架构.png")

## 配置演示
```yaml
name: v2shopapi   # 服务器名称
host: 127.0.0.1   # 服务器主机
port: 8101         # 服务器端口
tags:
  - v1
  - shop
  - api
jwt:
  key: jwt       # JWT签名密钥
sms:
  key: key    # 阿里短信API密钥
  secrect: secrect   # 阿里短信API密钥秘钥
redis:
  host: 127.0.0.1      # Redis服务主机
  port: 6379           # Redis服务端口
  expire: 3600         # Redis数据过期时间
consul:
  host: 127.0.0.1     # Consul服务主机
  port: 8500           # Consul服务端口
jaeger:
  host: 127.0.0.1
  port: 6831
  name: v1shop_api
oss:
  key: key
  secrect: secrect
  host: host
  callback_url: callback_url
  upload_dir: upload_dir
user-srv:
  #    host: 127.0.0.1   # 用户服务主机 直连使用
  #    port: 9090         # 用户服务端口 直连使用
  name: v1user_srv   # 用户服务名称
userop-srv:
  #    host: 127.0.0.1   # 用户服务主机直连使用
  #    port: 9090         # 用户服务端口直连使用
  name: v1uesrop_srv   # 用户服务名称
goods-srv:
  #    host: 127.0.0.1   # 用户服务主机直连使用
  #    port: 9090         # 用户服务端口直连使用
  name: v1goods_srv   # 用户服务名称
order-srv:
  #    host: 127.0.0.1   # 用户服务主机直连使用
  #    port: 9090         # 用户服务端口直连使用
  name: v1order_srv   # 用户服务名称
inventory-srv:
  #    host: 127.0.0.1   # 用户服务主机直连使用
  #    port: 9090         # 用户服务端口直连使用
  name: v1inventory_srv   # 用户服务名称

alipay:
  private_key: private_key+Q
  ali_public_key: ali_public_key+
  notify_url: notify_url
  return_url: return_urla
```

## 安装教程

服务器：Centos7


确保安装：Docker、Mysql、Consul、Nacos、Redis、Elasticsearch、Jaeger


可视化工具：Kibana(可选)、RESP(可选)

测试工具：ApiFox(可选)


若不想安装Docker请自行安装中间件

若不想安装Consul、Nacos请自行到微服务中改启动源码

若不想使用Elasticsearch(全文搜索)请改【商品服务-srv】核心逻辑

若不想使用Jaeger直接在main.go里注释掉相关信息

不演示安装Mysql和Redis

### 1、安装Nacos
https://blog.csdn.net/the_shy_faker/article/details/127523158

### 2、安装Consul
https://blog.csdn.net/the_shy_faker/article/details/127425125

### 3、安装Elasticsearch
https://blog.csdn.net/the_shy_faker/article/details/128520129

### 4、安装Jaeger
https://blog.csdn.net/the_shy_faker/article/details/129044832

## 使用说明

请点进server层或web层查看文档

ApiFox测试文档：https://nilszhai.apifox.cn

## 前端求救
本人不会vue，这是从网上拿的模板。路由在`src/api`或`src/apis`

[online-store](https://gitee.com/jzins/mxshop/tree/master/online-store)
[mall-master](https://gitee.com/jzins/mxshop/tree/master/mall-master)

