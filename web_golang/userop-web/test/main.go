// 请关闭中文输入法，用英文的字母和标点符号。
// 如果你想运行系统测试用例，请点击【执行代码】按钮，如果你想提交作答结果，请点击【提交】按钮，
// 注意：除答案外，请不要打印其他任何多余的字符，以免影响结果验证
// 本OJ系统是基于 OxCoder 技术开发，网址：www.oxcoder.com
// 模版代码提供基本的输入输出框架，可按个人代码习惯修改

package main

import (
	"fmt"
	"sort"
	"strconv"
)

func solution(n int, m int, arr []int) {

	// TODO: 请在此编写代码
	sort.Sort(sort.IntSlice(arr))
	flag := 0
	num := 0

	for i := len(arr) - 1; i >= 0; i-- {
		flag += 1
		if num += arr[i]; num > m {
			break
		}
	}
	if num < m || len(arr) != n {
		fmt.Println("No answer!!!")
	} else {
		fmt.Println(strconv.Itoa(flag))
	}

}

func main() {
	var tem_arr [2]int

	for i := 0; i < 2; i++ {
		fmt.Scan(&tem_arr[i])
	}

	n := tem_arr[0]
	m := tem_arr[1]

	arr := make([]int, n)

	for i := 0; i < n; i++ {
		fmt.Scan(&arr[i])
	}

	solution(n, m, arr)
}
