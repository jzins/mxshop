package main

import (
	"fmt"
	"github.com/smartwalle/alipay/v3"
	"net/url"
)

func main() {
	appID := "2021000122601241"
	privateKey := "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDIhdHkbi79LWoGaXjGhoJy8Ow2C7BG2BKmkxGS6EzNt1vln7bFmRIlZrBdqW6dR04NwkRSHCfZPeDliQ2Ro8u2+Q1BeO2HuIwHADEKJfLfIZqRC9sk6MTdfokK1r6CV1Kw4lRW/ttKt+nEaELKQ6/rlnBlVq4L1Z2xkhOVWjlt8MnlI85RanpO9TKrIs5srmBA0g2mm0zeal5hL9sTz7AE8veh99nqa3mwuL6aMEJJe9SNKLxKBaXuYsUxrjHJsRTgLRQ7eOPAtZeTulmUJWu7Jq+EDZNoQySp0sWlO7iosxpw8B0iiWk1diJ/Ik6QFEoWY+25xVxpUCD6H1pURXofAgMBAAECggEACoNZmcxdElELOgLVnjL3K+eNuteqCkzRzYHwbe9X94EhhEx3XKzF0sgrfv2fGwfeA8KnqlZnkHA6Fk61M2mPCKzOERyx92epj8Rmzc1nimNMo0Yn2yuJBsPXk4Xmvxas8zEqktUR0j0KOI3u0oPx+TwN8II25c/xUJoEBNSSJj1d1nO+nNZrI+tvoRE64NmRZ05+alraK54tU3wJXmar4Jd6Y7kH1cpFIZnn77R0h5o0SL3PA+agcuF/8XqAY7qNY2VgWfU6Avyxy8J7FMC3Fknqfgu0K7rlOPj5ggQFCugNacgTiH33u8sQufBhPSPW2aKPh7dNzP6Gjx2vVspBQQKBgQDjBQLd1paAJm2bF0WBVV7h1Ad5WfaHVvwr2509yIFatm/G4xU0Fvzfv+QvtTsjbaSDqPueomsZ4emFNxFEvBgmjFe0cbYaJCfrg4HRZbecPfe/OkNpVpscWcVbjDR7CJvJ8Y4DHOt8tLNMmRKsQruYkRy3y5AjTrN7CYZJ7jiPqQKBgQDiHuSo6VElyw6hE6bEcUeT0Ttmo5JBUltaCOX/KVQft8MK9N7OcdKEynUTsRKbbV6gPlIWrD/sMnmiszQuc5lxnAvuSq+3apUQdmvgwC0m4iNB8uboP58FV6MiWZXD0JY/X66YVymtyZFTSmlmSWRRJmcDGOlpqL5bnD1jgtb4hwKBgE9ftyrdFm3kAbqSHmkj8jK/MYXKYJndXfnvpUCu1BtlYhuF1ioKR4vrydqsYOr4MOcftCo4232HMWC0akNACKmhtCCRCXyGW7bKmh8ek137hJ3Fv0AbWcxFzu09TjQeRGoNHPwVpnhJrc8ATjkQdArZ4dKZR+IwaCyCngNHWVtRAoGAI+ZtvVeVt59eijpZftTRXWmZubj8jnxp3oiNI47HOWXuhH3OJg6yMr0GC4NQhfa+UEi0tb7RQ0ncP/WdskZONVhmGIYpEcDNO9jhVcSNJqA+osjS2krHj4yTbO+2NGmYgSVKzgsApgPbm9KzmzbJw4yKTFaXMFxHJLpQkM0EHl8CgYEA1wR6fkdR62uiU+rwT37WE2AHB5hKuiUPYlQ9YUKabEzIPCbj1h3rsXbedR71n82URGLZPOQxVnyZxPLAL0V93QyAbCUgfEGMe5drB37TZrDA+eONMJsJZpctrWiPFEZ2bcvbWlEg+G6ymYXZhcOuVrURR1c1AW+9T0KP5TrS88Q="
	aliPhublicKey := "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzMbTOmqp8ZWkmGdiDq+jGasF64g21aksOZvKIXsvxpdirT1imQAb6tuZPVUZCqu6ELiWggZn1DQ78wfvidQ022YRP6JKVaXFai10gNgaIlf1z8J+GbURSIx3OyK5iLE43iZWw89F+3xq17NkbHXzDSTu3iDBEKBVrKcSN7NwDjzCimeqKXE/Se7aA1tonAhvFX72GgXMFerHga56yk4FU1H8wVk3X/Cf+mgRZIuFzS2D3S2/LwDFS2iJQv73FZpNnFiK4aj6PagRDICx/WaIKIg3QItvLhoElKZG9GJLUnNhjcAV6xgf1Y6it/C0SgYVot95MdR//OylQ+e0eZFDbQIDAQAB"
	var client, err = alipay.New(appID, privateKey, false)
	if err != nil {
		panic(err)
	}
	err = client.LoadAliPayPublicKey(aliPhublicKey)
	if err != nil {
		panic(err)
	}
	//var p = alipay.TradeWapPay{}
	var p = alipay.TradePagePay{}
	p.NotifyURL = "https://www.baidu.com" //支付宝回调
	p.ReturnURL = "https://www.baidu.com" //支付后调转页面
	p.Subject = "Jzin-订单支付"               //标题
	p.OutTradeNo = "jzin"                 //传递一个唯一单号
	p.TotalAmount = "10.00"
	//p.ProductCode = "QUICK_WAP_WAY"
	p.ProductCode = "FAST_INSTANT_TRADE_PAY" //网页支付
	var url2 *url.URL
	url2, err = client.TradePagePay(p)
	if err != nil {
		fmt.Println(err)
	}

	var payURL = url2.String()
	fmt.Println(payURL)
	// 这个 payURL 即是用于支付的 URL，可将输出的内容复制，到浏览器中访问该 URL 即可打开支付页面。
}
